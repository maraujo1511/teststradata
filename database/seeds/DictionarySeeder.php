<?php

use Illuminate\Database\Seeder;

class DictionarySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('diccionario')->delete();

        $handle = fopen('archivoDiccionario.csv', "r");
        $header = true;
        $insert = [];
        $insert = true;
        while ($csvLine = fgetcsv($handle, 1000, ";")) {
            
            if ($header) {
                $header = false;
            } else {
               
                $insert = [
                    'departamento'  =>  $csvLine[0],
                    'localidad'     =>  $csvLine[1],
                    'municipio'     =>  $csvLine[2],
                    'nombre'        =>  $csvLine[3],
                    'anios_activo'  =>  (int)$csvLine[4],
                    'tipo_persona'  =>  $csvLine[5],
                    'tipo_cargo'    =>  $csvLine[6],
                ];
                

                // echo var_dump($insert);
                // break;
                if(!DB::table('diccionario')->insert($insert)){
                    $this->command->error('No se pudieron insertar todos los registros de archivoDiccionario.csv!');
                    $insert = false;
                    break;
                }
            }

        }
        
        if ($insert)
            $this->command->info('Insertados todos los registros');
    }
}
