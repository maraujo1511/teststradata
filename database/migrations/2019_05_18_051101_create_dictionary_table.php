<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDictionaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diccionario', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('departamento');
            $table->string('localidad');
            $table->string('municipio');
            $table->string('nombre');
            $table->integer('anios_activo');
            $table->string('tipo_persona');
            $table->string('tipo_cargo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diccionario');
    }
}
