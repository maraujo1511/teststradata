# Test StraData
*Prueba para la empresa Stradata realizada con **Lravel 5***

# Base de Datos
- MySql
- DB: **stradata**
- Host: **localhost**

# Ejecutar el Test
*Instalar el proyecto*

-  `composer install`

*Crear e insertar los datos en la base de datos*

- `php artisan migrate --seed`

*Consumo del API en local*

- `localhost/api/dictionary`

# Informe sobre el test

Se usaron los algoritmos de levenshtein y de soundtext con el encontrar una coincidencia con más sentido a nivel porcentual

El api responde usando ciertos criterios

  Se verifica si la palabra a comprar tiene en su composición alguna de estas letras, o conjunto de letras:
    z, s, b, v, x, j, c, k, ph, f, ji, gi, je, ge, t, th.
  
  Esto con la finalidad de poder detectar si nivel fonético tienen algún parecido esto se realiza con el algoritmo de soundtext()

  Si lo anterior no sucede, se procede a realizar la comparación con el algoritmo de levenshtein

  El calculo se realiza de la siguiente manera:

  - Si es para soundtext()
      Se verifica que la palabra a buscar y la contenida en base de datos, comience con alguna de las letras o conjunto de letras ya nombrada, en la misma oportunidad de comparación, esto con la finalidad de poder comprar solo el número que retorna el algoritmo de soundtext().

      Si lo anterior no sucede comparo el resultado de soundtext() para cada palabra exactamente.

      Para los dos casos anteriores si la comparacion coincide, decido darle un 100% a esa comparación

  - Si es para levenshtein()
      Se toman las siguientes decisiones:

      + Se compra la cantidad de letras entre la que se va a buscar y la que se va a comparar para así obtener la mayor
      + Se llama a la función para comprar los nombres. Nota: el número máximo que puede retornar este algoritmo es la cantidad de letras que contenga la mayor de las palabras a comprar, si y solo si las palabras no tienen letras parecidas
      + Se resta el tamaño de la palabra mayor a comparar con el resultado de la función, este resultado de multiplica por 100, para llevarlo a porcentaje.
      + El resultado anterior se divide entre el tamaño de la palabra mayor para obtener el resultado certero
  
  - Calculo total
      Estos cálculos se realizan permutando el nombre y el apellido de la base de datos con el nombre y el apellido de entrada, llevando un registro de los cálculos ya nombrados.

      Al final se suman los porcentajes y de divide entre la cantidad de nombres y apellidos que componen el nombre propio de la base datos, y este es el porcentaje final