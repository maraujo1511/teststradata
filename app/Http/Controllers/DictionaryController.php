<?php

namespace App\Http\Controllers;

use App\Dictionary;
use Illuminate\Http\Request;

class DictionaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ret = [];
        $arr_insert = [];
        // Valido que los parametros de entrada sean correctos
        if ($request->get('porcentaje') == NULL || $request->get('name') == NULL) {

            $ret[] =[
                'error'=> true,
                'mensaje'=> "Falta parametro de entrada: {'name':<nombre apellido>, 'porcentaje': <int> > 0}"
            ];
        } else if (sizeof(explode(' ', $request->get('name'))) == 1) {
            $ret[] =[
                'error'=> true,
                'mensaje'=> "El formato del parametro 'name' no es correcto: <Nombre Apellido> o <Apellido Nombre>"
            ];
        } else if ($request->get('porcentaje') <= 0 || $request->get('porcentaje') > 100) {
            $ret[] =[
                'error'=> true,
                'mensaje'=> "El formato del parametro 'porcentaje' no es correcto: debe estar entre 1 y 100"
            ];
        } else {
            $max_len = 0;
            $porcentaje = $request->get('porcentaje');
            // divido el nombre y el apellido
            $full_name = explode(' ', $request->get('name'));

            // Nombre
            $name1 = $full_name[0];
            // Apellido
            $name2 = $full_name[1];

            // Obtengo todos los datos de la DB
            $dict = Dictionary::all();

            foreach ($dict as $d) {
                $por_name_db = 0;
                $arr_por = [];
                // Arreglos Auxiliares
                $arr_insert[$name1] = [];
                $arr_insert[$name2]= [];

                // divido el nombre de la DB
                $full_name_db = explode(' ', $d->nombre);
                $tam_name_db = sizeof($full_name_db);

                // Se realiza la compracion para el $name1
                for ($i = 0; $i < $tam_name_db; $i++) {
                    $soundtext = false;

                    // valido si la palabra tienen z, s, b, v, x, j, c, k, ph, f, ji, gi, je, ge, t, th
                    // con la finalidad de comparar foneticamente
                    if (
                        strstr(strtolower($full_name_db[$i]), "z")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "s")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "b")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "v")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "x")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "j")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "c")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "k")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "ph")    != ""   ||
                        strstr(strtolower($full_name_db[$i]), "f")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "ji")    != ""   ||
                        strstr(strtolower($full_name_db[$i]), "gi")    != ""   ||
                        strstr(strtolower($full_name_db[$i]), "je")    != ""   ||
                        strstr(strtolower($full_name_db[$i]), "ge")    != ""   ||
                        strstr(strtolower($full_name_db[$i]), "t")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "th")    != ""
                    ) {
                        // Obtengo la primera letra de la palabra de DB y de la de entrada
                        $first_c_db = $full_name_db[$i][0];
                        $second_c_db = $full_name_db[$i][1];
                        $first_c_in = $name1[0];
                        $second_c_in = $name1[1];

                        // Valido que la palabra este compuesta por una de las siguientes letras en su primera posicion
                        if (
                            (strtolower($first_c_in) == 's' && strtolower($first_c_db) == 's') || (strtolower($first_c_in) == 'z' && strtolower($first_c_db) == 'z') || (strtolower($first_c_in) == 'b' && strtolower($first_c_db) == 'b') || (strtolower($first_c_in) == 'v' && strtolower($first_c_db) == 'v') || (strtolower($first_c_in) == 'x' && strtolower($first_c_db) == 'x') || (strtolower($first_c_in) == 'j' && strtolower($first_c_db) == 'j') || (strtolower($first_c_in) == 'c' && strtolower($first_c_db) == 'c') || (strtolower($first_c_in) == 'k' && strtolower($first_c_db) == 'k') || (strtolower($first_c_in) == 'f' && strtolower($first_c_db) == 'f') || (
                                (strtolower($first_c_in) == 'p' && strtolower($second_c_in) == 'h') && (strtolower($first_c_db) == 'p' && strtolower($second_c_db) == 'h'))   || (
                                (strtolower($first_c_in) == 'j' && strtolower($second_c_in) == 'i') && (strtolower($first_c_db) == 'j' && strtolower($second_c_db) == 'i'))   || (
                                (strtolower($first_c_in) == 'g' && strtolower($second_c_db) == 'i') && (strtolower($first_c_db) == 'g' && strtolower($second_c_db) == 'i'))   || (
                                (strtolower($first_c_in) == 'j' && strtolower($second_c_db) == 'e') && (strtolower($first_c_db) == 'j' && strtolower($second_c_db) == 'e'))   || (
                                (strtolower($first_c_in) == 'g' && strtolower($second_c_in) == 'e') && (strtolower($first_c_db) == 'g' && strtolower($second_c_db) == 'e'))   || (strtolower($first_c_db) == 't' && strtolower($second_c_db) == 'h')

                        ) {

                            // Llamo a la funcion fonetica de php soundtext()
                            // Extraigo la primera letra del resultado de soundtext()
                            $fone_name_db = substr(soundex($full_name_db[$i]), 1);
                            $fone_name = substr(soundex($name1), 1);
                            if ($fone_name_db == $fone_name) {
                                $soundtext = true;
                                if (!in_array(100, $arr_insert[$name1])) {

                                    $arr_insert[$name1][] = 100;
                                    $arr_por[] = 100;
                                    $por_name_db += 100;
                                }
                            }
                        // Sino esta compuesta por las letras anteriores en la priemera posicion
                        // valido sin la extraccion de la priemra leta a nivel de  fonetica
                        } else {
                            $fone_name_db = soundex($full_name_db[$i]);
                            $fone_name = soundex($name1);
                            if ($fone_name_db == $fone_name) {
                                $soundtext = true;
                                if (!in_array(100, $arr_insert[$name1])) {
                                    $arr_insert[$name1][] = 100;
                                    $arr_por[] = 100;
                                    $por_name_db += 100;
                                }
                            }
                        }
                    }
                    // Valido sino obtuve resultado foneticamente al 100%
                    // Para asi aplicar levenshtein
                    if (!$soundtext) {

                        // busco el mayor entre los nombres a compara
                        $max_len = strlen($name1) >= strlen($full_name_db[$i]) ?  strlen($name1) : strlen($full_name_db[$i]);

                        // llamada a la funcion levenshtein para comprar los nombres
                        $res_lev = levenshtein($name1, $full_name_db[$i]);
                        //  $max_len es la palabra con mayor numero de caracteres
                        //  $res_lev es el resultado de aplicar la funcion levenshtein
                        //  Como el mayor valor que puede retornar levenshtein depende de la palabra que
                        //      tenga mayor numero de caracteres, elegimos ese número como nuestro
                        //      punto mas alto a comparar es decir nuestro 100%
                        //  (maxima_palabra - resultado de levenshtein) * 100 -> se calcula la diferencia
                        //      entre la maxima palabra y el resultado de levenshtein para asi poder enmarcar
                        //      un resultado certero segun el criterio ya mensionado. Se multiplica por
                        //      100 para llevarlo a terminos porcentuales y luego divirdo entre el $max_len
                        //      para obtener el porcentaje real
                        //  Si $max_len es igual a $res_lev el porcentaje es cero 0 porque las palabras a
                        //      comparar no tienen nada en comun
                        $arr_por[] = (($max_len - $res_lev) * 100) / $max_len;
                        $por_name_db += (($max_len - $res_lev) * 100) / $max_len;
                        $arr_insert[$name1][] = (($max_len - $res_lev) * 100) / $max_len;
                    }
                }


                // Se realiza la compracion para el $name2
                // Se repite el algoritmo anterior c
                for ($i = 0; $i < $tam_name_db; $i++) {
                    $soundtext = false;

                    if (
                        strstr(strtolower($full_name_db[$i]), "z")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "s")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "b")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "v")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "x")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "j")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "c")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "k")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "ph")    != ""   ||
                        strstr(strtolower($full_name_db[$i]), "f")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "ji")    != ""   ||
                        strstr(strtolower($full_name_db[$i]), "gi")    != ""   ||
                        strstr(strtolower($full_name_db[$i]), "je")    != ""   ||
                        strstr(strtolower($full_name_db[$i]), "ge")    != ""   ||
                        strstr(strtolower($full_name_db[$i]), "t")     != ""   ||
                        strstr(strtolower($full_name_db[$i]), "th")    != ""
                    ) {
                        
                        $first_c_db = $full_name_db[$i][0];
                        $second_c_db = $full_name_db[$i][1];
                        $first_c_in = $name2[0];
                        $second_c_in = $name2[1];

                        
                        if (
                            (strtolower($first_c_in) == 's' && strtolower($first_c_db) == 's') || (strtolower($first_c_in) == 'z' && strtolower($first_c_db) == 'z') || (strtolower($first_c_in) == 'b' && strtolower($first_c_db) == 'b') || (strtolower($first_c_in) == 'v' && strtolower($first_c_db) == 'v') || (strtolower($first_c_in) == 'x' && strtolower($first_c_db) == 'x') || (strtolower($first_c_in) == 'j' && strtolower($first_c_db) == 'j') || (strtolower($first_c_in) == 'c' && strtolower($first_c_db) == 'c') || (strtolower($first_c_in) == 'k' && strtolower($first_c_db) == 'k') || (strtolower($first_c_in) == 'f' && strtolower($first_c_db) == 'f') || (
                                (strtolower($first_c_in) == 'p' && strtolower($second_c_in) == 'h') && (strtolower($first_c_db) == 'p' && strtolower($second_c_db) == 'h'))   || (
                                (strtolower($first_c_in) == 'j' && strtolower($second_c_in) == 'i') && (strtolower($first_c_db) == 'j' && strtolower($second_c_db) == 'i'))   || (
                                (strtolower($first_c_in) == 'g' && strtolower($second_c_db) == 'i') && (strtolower($first_c_db) == 'g' && strtolower($second_c_db) == 'i'))   || (
                                (strtolower($first_c_in) == 'j' && strtolower($second_c_db) == 'e') && (strtolower($first_c_db) == 'j' && strtolower($second_c_db) == 'e'))   || (
                                (strtolower($first_c_in) == 'g' && strtolower($second_c_in) == 'e') && (strtolower($first_c_db) == 'g' && strtolower($second_c_db) == 'e'))   || (strtolower($first_c_db) == 't' && strtolower($second_c_db) == 'h')

                        ) {
                            
                            $fone_name_db = substr(soundex($full_name_db[$i]), 1);
                            $fone_name = substr(soundex($name2), 1);
                            if ($fone_name_db == $fone_name) {
                                $soundtext = true;
                                if (!in_array(100, $arr_insert[$name2])) {
                                    $arr_insert[$name2][] = 100;
                                    $arr_por[] = 100;
                                    $por_name_db += 100;
                                }
                            }
                           
                        } else {
                            $fone_name_db = soundex($full_name_db[$i]);
                            $fone_name = soundex($name2);
                            if ($fone_name_db == $fone_name) {
                                $soundtext = true;
                                if (!in_array(100, $arr_insert[$name2])) {
                                    $arr_insert[$name2][] = 100;
                                    $arr_por[] = 100;
                                    $por_name_db += 100;
                                }
                            }
                        }
                    }
                    
                    if (!$soundtext) {
                        
                        $max_len = strlen($name2) >= strlen($full_name_db[$i]) ?  strlen($name2) : strlen($full_name_db[$i]);
                        
                        $res_lev = levenshtein($name2, $full_name_db[$i]);
                        
                        $por_name_db += (($max_len - $res_lev) * 100) / $max_len;
                        $arr_por[] = (($max_len - $res_lev) * 100) / $max_len;
                        $arr_insert[$name2][] = (($max_len - $res_lev) * 100) / $max_len;
                    }
                }

                // contar las coicidencias del 100% para el $name1 y $name2
                $k = 0;
                for ($i = 0; $i < sizeof($arr_por); $i++) {
                    if ($arr_por[$i] == 100) {
                        $k++;
                    }
                }

                // Si encuentro en el arr_por 2 veces 100 entonces la coincidencia del nombre completo es exacta
                if ($k >= 2) {
                    $res_por = 100;
                } else {
                    // Sino se realiza el calculo de todos los porcentajes obtenedos en el caldulo
                    // y se divide entre la cantidad de palabras que contiene el nombrede la DB
                    $res_por = $por_name_db / $tam_name_db;
                }

                // Valido que el porcentaje de entrada sea mayor o igal al resultante
                if ($res_por >= $porcentaje) {

                    $ret[] = [
                        'nombre_a_comparar' => $d->nombre,
                        'nombre_busqueda' => $request->get('name'),
                        'porcentaje' => number_format($res_por, 2, '.', '') . "%"
                    ];
                }
            }
        } // End primer IF

        return $ret;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { }

    /**
     * Display the specified resource.
     *
     * @param  $name
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    { }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dictionary  $dictionary
     * @return \Illuminate\Http\Response
     */
    public function edit(Dictionary $dictionary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dictionary  $dictionary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dictionary $dictionary)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dictionary  $dictionary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dictionary $dictionary)
    {
        //
    }
}
