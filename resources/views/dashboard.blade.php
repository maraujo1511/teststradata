@extends('layouts.app', ['activePage' => 'dashboard', 'titlePage' => __('Dashboard')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <form id="search" method="post" action="{{ route('dictionary') }}" autocomplete="off" class="form-horizontal">
          @csrf
          @method('get')

          <div class="card ">
            <div class="card-header card-header-primary">
              <h4 class="card-title">{{ __('Buscar Coincidencia') }}</h4>

            </div>
            <div class="card-body ">

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <input class="form-control" name="name" id="input-name" type="text" placeholder="{{ __('Nombre') }}" value="" required="true" aria-required="true" />
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <input class="form-control" name="porcentaje" id="input-porcentaje" type="number" placeholder="{{ __('Porcentaje') }}" value="" required="true" aria-required="true" />
                  </div>
                </div>
              </div>

            </div>
            <div class="card-footer ml-auto mr-auto">
              <button type="submit" class="btn btn-primary">{{ __('Buscar') }}</button>
            </div>
        </div>
      </form>
    </div>
  </div>
  @if (isset($dictionary))
  {{-- <p>{{ var_dump($dictionary)}}</p> --}}

  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header card-header-success">
          <h4 class="title">Resultado de la busqueda para:<strong> {{$dictionary[0]['nombre_busqueda']}} </strong></h4>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <div class="card-content table-responsive">
              <table class="table">
                <thead class="text-primary">
                  <th>Nombre a Comparar</th>
                  <th>% Coincidencia</th>
                </thead>
                <tbody>
                  @foreach ($dictionary as $item)
                  <tr>
                    <td>{{$item['nombre_a_comparar']}}</td>
                    <td class="text-primary">{{$item['porcentaje']}}</td>
                  </tr>

                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>


    </div>
    @endif
  </div>
</div>
@endsection

@push('js')
<script>
  $(document).ready(function() {
    // Javascript method's body can be found in assets/js/demos.js
    md.initDashboardPageCharts();
  });
  $("#search").submit(function() {
    var tam = $("#input-name").val().split(" ").length
    if (tam == 1 || tam >=3) {
      md.showNotification('top', 'right', 'danger', 'Error.<br> El dato de entrada debe ser <br> Nombre Apellido o Apellido Nombre');
      return false;
    }

    if ($("#input-porcentaje").val() == 0){
      md.showNotification('top', 'right', 'danger', 'Error.<br> Debe agregar un porcentaje mayor a cero ( 0 )');
      return false;
    }

  });
</script>
@endpush